import guess.guess_number as guess_number
import unittest
import unittest.mock as mock

class TestGuessNumber(unittest.TestCase):
    @mock.patch('random.randint',return_value=3)
    def test_guess_int_1_to_10_should_be_3(self, guess_value):
        self.assertEqual(guess_number.guess_int(1,10),3,'should be 3')
        
    @mock.patch('random.randint',return_value=8125)
    def test_guess_int_1_to_10000_should_be_8125(self, guess_value):
        self.assertEqual(guess_number.guess_int(1,10000),8125,'should be 8125')

    @mock.patch('random.randint',return_value=0)
    def test_guess_int_0_to_0_should_be_0(self, guess_value):
        self.assertEqual(guess_number.guess_int(0,0),0,'should be 0')
    
    @mock.patch('random.randint',return_value=7)
    def test_guess_int_10_to_1_should_be_7(self, guess_value):
        self.assertEqual(guess_number.guess_int(10,1),7,"should be 7")

    @mock.patch('random.randint',return_value=80)
    def test_guess_int_80_to_80_should_be_80(self, guess_value):
        self.assertEqual(guess_number.guess_int(80,80),80,"should be 80")

    @mock.patch('random.randint',return_value=-61)
    def test_guess_int_minus_100_to_0_should_be_minus_61(self, guess_value):
        self.assertEqual(guess_number.guess_int(-100,0),-61,"should be -61")

    @mock.patch('random.randint',return_value=0)
    def test_guess_int_minus_910_to_10_should_be_0(self, guess_value):
        self.assertEqual(guess_number.guess_int(-10,10),0,"should be 0")

    @mock.patch('random.uniform',return_value=4.150653452856887)
    def test_guess_float_1_to_10_should_be_4_point_150653452856887(self, guess_value):
        self.assertEqual(guess_number.guess_float(1,10),4.150653452856887,"should be 4.150653452856887")

    @mock.patch('random.randint',return_value=0.5)
    def test_guess_float_0_point_5_to_0_point_5_should_be_0_point_5(self, guess_value):
        self.assertEqual(guess_number.guess_int(0.5,0.5),0.5,"should be 0.5")
    
    @mock.patch('random.uniform',return_value=0.2378775206073871)
    def test_guess_float_0_point_1_to_1_point_0_should_be_0_point_2378775206073871(self, guess_value):
        self.assertEqual(guess_number.guess_float(0.1,1.0),0.2378775206073871,"should be 0.2378775206073871")

    @mock.patch('random.uniform',return_value=-5.249932623055033)
    def test_guess_float_minus_1_to_minus_10_should_be_minus_5_point_249932623055033(self, guess_value):
        self.assertEqual(guess_number.guess_float(-1,-10),-5.249932623055033,"should be -5.249932623055033")
    